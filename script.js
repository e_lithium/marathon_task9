'use strict'

function start(){
    let numberOfFilms = NaN;
    while ((isNaN(numberOfFilms)) || (numberOfFilms === null) || (numberOfFilms === '')){
        numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?', '');
    }
    console.log(numberOfFilms);
    return numberOfFilms;
}

function rememberMyFilms(){
    let movieCounter = 0;
    while (movieCounter < 2){
        const movie = prompt('Один из последних просмотренных фильмов?', '');
        const movieRating = +prompt('На сколько оцените его?', '');
        if ((movie !== null) && (movieRating !== null) && !(isNaN(movieRating)) && (movie != '') && (movieRating !== '') && (movie.length <= 50)){
            personalMovieDB.movies[movie] = +movieRating;
            movieCounter++;
        }
    }
}

function detectPersonalLevel(){
    if (personalMovieDB.count < 10){
        alert("Просмотрено довольно мало фильмов")
    } else if (personalMovieDB.count <= 30) {
        alert("Вы классический зритель");
    } else if (personalMovieDB.count > 30) {
        alert("Вы киноман");
    } else {
        alert("Произошла ошибка");
    }
}

function showMyDB(){
    if (personalMovieDB.privat == false){
        console.log(personalMovieDB);
    }
}

function writeYourGenres(){
    let genreNum = 1;
    while (genreNum != 4){
        const genre = prompt(`Ваш любимый жанр под номером ${genreNum}?`, '');
        if ((genre !== null) && (genre != '')){
            personalMovieDB.genres[genreNum - 1] = genre;
            genreNum++;
        }
    }
}

let personalMovieDB = {
    count: start(),
    movies: {},
    actors: {},
    genres: [],
    privat: false
};

rememberMyFilms();
detectPersonalLevel();
showMyDB();
writeYourGenres();
